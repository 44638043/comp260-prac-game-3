﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class shellMenu : MonoBehaviour {

	public GameObject shellPanel;

	public GameObject optionsPanel;
	public Dropdown qualityDropdown;
	public Dropdown resolutionDropdown;
	public Toggle fullscreenToggle;
	public Slider volumeSlider;
	public Slider musicSlider;

	public AudioSource bgm;

	private bool paused = true;
	// Use this for initialization
	void Start () {
		SetPaused(paused);
		optionsPanel.SetActive(false);

		qualityDropdown.ClearOptions();
		List<string> names = new List<string>();
		for (int i = 0;i < QualitySettings.names.Length; i++) {
			names.Add(QualitySettings.names[i]);
		}
		qualityDropdown.AddOptions(names);
		
		resolutionDropdown.ClearOptions();
		List<string> resos = new List<string>();
		for (int i = 0;i < Screen.resolutions.Length; i++) {
			resos.Add(Screen.resolutions[i].ToString());
		}
		resolutionDropdown.AddOptions(resos);

		if (PlayerPrefs.HasKey("AudioVolume")) {
			AudioListener.volume = PlayerPrefs.GetFloat("AudioVolume");
		} else {
			AudioListener.volume = 1;
		}

		bgm.ignoreListenerVolume = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (!paused && Input.GetKeyDown(KeyCode.Escape)){
			SetPaused(true);
		}
	}

	private void SetPaused(bool p) {
		paused = p;
		shellPanel.SetActive(paused);
		Time.timeScale = paused ? 0 : 1;
	}

	public void OnPressedPlay() {
		SetPaused (false);
	}

	public void OnPressedQuit() {
		Application.Quit();
	}
	
	public void OnPressedOptions() {
		shellPanel.SetActive(false);
		optionsPanel.SetActive(true);

		qualityDropdown.value = QualitySettings.GetQualityLevel();

		int currentReso = 0;
		for (int i = 0; i < Screen.resolutions.Length; i ++) {
			if (Screen.resolutions[i].width == Screen.width &&
			    Screen.resolutions[i].height == Screen.height) {
				currentReso = i;
				break;
			}
		}
		resolutionDropdown.value = currentReso;

		fullscreenToggle.isOn = Screen.fullScreen;

		volumeSlider.value = AudioListener.volume;

		musicSlider.value = bgm.volume;
	}

	public void OnPressedCanel() {
		shellPanel.SetActive(true);
		optionsPanel.SetActive(false);
	}
	
	public void OnPressedApply() {
		shellPanel.SetActive(true);
		optionsPanel.SetActive(false);

		QualitySettings.SetQualityLevel(qualityDropdown.value);

		Resolution res = Screen.resolutions[resolutionDropdown.value];
		Screen.SetResolution(res.width, res.height, true);
		
		Screen.SetResolution(res.width, res.height, fullscreenToggle.isOn);

		AudioListener.volume = volumeSlider.value;

		bgm.volume = musicSlider.value;

		PlayerPrefs.SetFloat("AudioVolume", AudioListener.volume);
	}
}
