﻿using UnityEngine;
using System.Collections;

public class fireBullet : MonoBehaviour {

	public bulletMove bulletPrefab;
	public float timer;


	public Animator anim;

	// Use this for initialization
	void Start () {
		timer = 1;
		anim.speed = 1;
	}
	
	// Update is called once per frame
	void Update () {

		if (Time.timeScale == 0) {
			return;
		}

		timer += Time.deltaTime;
		if (Input.GetButtonDown("Fire1") && timer > 1) {
			bulletMove bullet = Instantiate(bulletPrefab);

			bullet.transform.position = transform.position;

			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

			bullet.dir = ray.direction;

			timer = 0;
		}
	}
}
