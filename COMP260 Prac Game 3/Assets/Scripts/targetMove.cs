﻿using UnityEngine;
using System.Collections;

public class targetMove : MonoBehaviour {


	private Animator anim;
	public float startTime = 0;

	// Use this for initialization
	void Start () {
		// get the animator component
		anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		//set the Start parameter to true
		// if we have passed the start time
			anim.SetBool("Start", Time.time >= startTime);
	}

	void destroy() {
		Destroy(gameObject);
	}

	void OnCollisionEnter (Collision col) {
		// transition to die animation
		anim.SetBool("Hit", true);
	}
}
