﻿using UnityEngine;
using System.Collections;

public class bulletMove : MonoBehaviour {

	public float speed = 10.0f;
	public Vector3 dir;
	private Rigidbody rbody;

	public float timer;

	// Use this for initialization
	void Start () {
		rbody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		rbody.velocity = speed * dir;
		timer += Time.deltaTime;
		if (timer >= 3.0f) {
			Destroy(gameObject);
		}
	}

	void Update() {
	}

	void OnCollisionEnter() {
		Destroy(gameObject);
	}
}
